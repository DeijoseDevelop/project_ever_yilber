from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass


class Player(models.Model):
    name = models.CharField(max_length=50)
    born_date = models.PositiveIntegerField()
    age = models.PositiveIntegerField()
    goals = models.PositiveIntegerField()
    assists = models.PositiveIntegerField()
    country = models.CharField(max_length=20)
    position = models.CharField(max_length=10)
    played_games = models.PositiveIntegerField()
    titular = models.PositiveIntegerField()
    minutes = models.FloatField()
    goals_without_penal = models.PositiveIntegerField()
    executed_penal = models.PositiveIntegerField()
    yellow_cards = models.PositiveIntegerField()
    red_cards = models.PositiveIntegerField()
    coverage_goals = models.FloatField()
    coverage_assists = models.FloatField()
    coverage_goals_assists = models.FloatField()
    coverage_goals_without_penal = models.FloatField()
    coverage_goals_assists_without_penal = models.FloatField()

    def __str__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    position = models.PositiveIntegerField()
    played_matches = models.PositiveIntegerField()
    win_matches = models.PositiveIntegerField()
    draw_matches = models.PositiveIntegerField()
    defeat_matches = models.PositiveIntegerField()
    goals_scored = models.PositiveIntegerField()
    goals_against = models.PositiveIntegerField()
    goals_difference = models.PositiveIntegerField()
    points = models.PositiveIntegerField()
    points_per_match = models.FloatField()
    people_assisted = models.PositiveIntegerField()
    goal_scorer = models.CharField(max_length=50)
    notes = models.CharField(max_length=30)
    technical_manager = models.CharField(max_length=30)
    technical_assistant = models.CharField(max_length=30)
    player_id = models.ForeignKey(Player, on_delete=models.CASCADE)

    def __str__(self):
        return self.name