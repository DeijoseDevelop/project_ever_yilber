from random import Random
from .models import *
from .serializers import *
from django.contrib.auth import authenticate, login, logout
#from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, status
#from rest_framework import permissions
from django.shortcuts import render
from django.http import Http404
from utils.random_number import random_hundred, random_countries,born_date
import time
from utils.validate_notes import validate_notes



class TeamListAPIView(APIView):
    def get(self, request):
        teams = Team.objects.all()
        serializer = TeamSerializer(teams, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = TeamSerializer(data=request.data)
        request.data['people_assisted'] = random_hundred()
        time.sleep(2)
        validate_notes(request)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class TeamDetailAPIView(APIView):
    def get_object(self, pk):
        try:
            return Team.objects.get(pk=pk)
        except Team.DoesNotExist:
            return Http404

    def get(self, request, pk):
        team = self.get_object(pk)
        serializer = TeamSerializer(team)
        return Response(serializer.data)

    def put(self, request, pk):
        team = self.get_object(pk)
        serializer = TeamSerializer(team, data=request.data)
        request.data['people_assisted'] = random_hundred()
        time.sleep(2)
        validate_notes(request)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        team = self.get_object(pk)
        team.delete()



class LoginView(APIView):
    """
    # "If the user is authenticated, log them in and return their data, otherwise return a 404."

    # The first thing we do is get the username and password from the request.data. We then use Django's
    # authenticate method to check if the user is valid. If the user is valid, we log them in using
    # Django's login method and return their data. If the user is not valid, we return a 404
    """
    def post(self, request):
        """
        If the user is authenticated, log them in and return their data, otherwise return a 404

        :param request: The request object is a standard Django request object
        :return: The user object is being returned.
        """
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return Response(
                UserSerializer(user).data,
                status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_404_NOT_FOUND)

class LogoutView(APIView):
    def post(self, request):
        """
        It logs out the user and returns a 200 OK response

        :param request: The request object
        :return: The status code 200 is being returned.
        """
        logout(request)
        return Response(status=status.HTTP_200_OK)


class SignupView(generics.CreateAPIView):
    serializer_class = UserSerializer




class PlayerAPIView(APIView):

    def get(self, request):
        player = Player.objects.all()
        player_serializer = PlayerSerializer(player, many=True)
        return Response(player_serializer.data)


    def post(self, request):
        serializer = PlayerSerializer(data =request.data) 
        request.data['born_date']= born_date()
        request.data['country']= random_countries()
        time.sleep(2)       
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PlayerDetailApi(APIView):
    def get_object(self, pk):
        try:
            return Player.objects.get(pk=pk)
        except Player.DoesNotExist:
            return Http404

    def get(self, request, pk):
        player = self.get_object(pk)
        serializer = PlayerSerializer(player)
        return Response(serializer.data)

    def put(self, request, pk):
        player = self.get_object(pk)
        serializer = PlayerSerializer(player, data=request.data)
        request.data['born_date']= born_date()
        request.data['country']= random_countries()
        time.sleep(2)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        player = self.get_object(pk)
        player.delete()



