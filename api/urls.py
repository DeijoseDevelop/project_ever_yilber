from django.urls import path
#from rest_framework.routers import DefaultRouter
from .views import LoginView, LogoutView, SignupView, TeamListAPIView, TeamDetailAPIView


#router = DefaultRouter()
#router.register(r'teams', TeamListAPIView, basename='teams')
#router.register(r'teams/(?P<pk>[0-9]+)', TeamDetailAPIView, basename='team')


urlpatterns = [
    path('teams/', TeamListAPIView.as_view(), name='teams'),
    path('teams/<int:pk>/', TeamDetailAPIView.as_view(), name='team'),
    path('auth/login/', LoginView.as_view(), name='auth-login'),
    path('auth/logout/', LogoutView.as_view(), name='auth-logout'),
    path('auth/signup/', SignupView.as_view(), name='auth-signup'),
] #+ router.urls

